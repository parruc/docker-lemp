
build:
	docker-compose build --no-cache wsgi

up:
	docker-compose up -d

down:
	docker-compose down

recreate: down up

rebuild: down chown build up

restart:
	docker-compose restart